//
//  AddProductSheet.swift
//  Swift Data Tut
//
//  Created by Omar Abdulrahman on 25/10/2023.
//

import SwiftUI

struct AddProductSheet: View {
    
    @Environment(\.modelContext) var context
    @Environment(\.dismiss) private var dismiss
    @Environment(\.presentationMode) var presentationMode

    @State private var name: String = ""
    @State private var date: Date = .now
    @State private var price: Double = 0
    
    var body: some View {
        NavigationStack {
            Form {
                TextField("Expense Name", text: $name)
                DatePicker("Date", selection: $date, displayedComponents: .date)
                TextField("Price", value: $price, format: .currency(code: "USD"))
                    .keyboardType(.decimalPad)
            }

            .navigationTitle("New Product")
            .navigationBarTitleDisplayMode(.large)
            .toolbar {
                ToolbarItemGroup(placement: .topBarLeading) {
                    Button("Cancel") { dismiss() }
                    }
                    ToolbarItemGroup(placement: .topBarTrailing) {
                        Button("Save") {
                            // Save code goes here
                            let product = Product(name: name, date: date, price: price)
                            context.insert(product)
                            dismiss()
                            
                    }
                }
            }
        }
    }
}

#Preview {
    AddProductSheet()
}
