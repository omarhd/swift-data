//
//  ProductCell.swift
//  Swift Data Tut
//
//  Created by Omar Abdulrahman on 25/10/2023.
//

import SwiftUI

struct ProductCell: View {
    
    let product: Product
    
    var body: some View {
        HStack {
            Text(product.name)
            Text(product.date, format: .dateTime.month(.abbreviated).day())
                .frame(width: 70, alignment: .leading)

            Spacer()

            Text((product.price), format: .currency(code: "USD"))
        }
    }
}

#Preview {
    ProductCell(product: Product(name: "iPhone", date: Date(), price: 1200.0))
}
